# Sublime-Text

Sublime-Text have many helpful _packages_, this can be installed using the **Package Control**, which can be done using:
1. **Install Package Control** from Tools > Command Palette (CTRL + Shift + P in Linux)
2. Install the Following Packages (Command Palette > Install Package):
	- BracketHighlighter : Highlights Brackets in the _gutter_ for Easy Tracking
	- SideBarEnhancements : Gives some extra commands under _right-click_ in sidebar menu
	- Anaconda : Python-Specific, gives great control on Code-Linting, jumping to-and-from function definations, etc. [Official Documentation](http://damnwidget.github.io/anaconda)
	
	_Note:_ Anaconda user-settings are not changed, but can be changed to (i) autoformat, (ii) PEP Compliance Checks, etc.

    - A File Icon : Great, shows Icons Associated with each File!
    - Terminus : Open Terminal in Sublime Text3
    - Origami : Move Panel Below - like Atom/VS Code
	- DotENV : Syntax Highlighting for `.env` Files


- **Anaconda Package** gives many commands to Sublime, however by default code-linting is turned on which produces white-border around python codes. To remove these, update the anaconda-settings file with `Anaconda.sublime-settings` which is available in sublime package directory or you can directly access this file from `Preferences > Package Settings > Anaconda > Settings - User` and add the following lines:
```python
{
  "anaconda_linting": false,
}
```

Directory or Commands
```bash
cd ~/.config/sublime-text-3/Packages/User/ # Contains user Settings for Sublime-Text
touch Preferences.sublime-settings # if it does not exists, or directly replace the file with the one here
```
