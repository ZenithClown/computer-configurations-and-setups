
<div align="center">

# Internet Download Manager

🔰 `IDMan` - *"... a tool to increase download speeds by up to 5 times, resume and schedule downloads ..."*

|| Single 💻 | Lifetime 👤 License | 📧 `dpramanik.official` ||

</div>

<div align = "justify">

Well, there aren't any automated scripts/online backup service for settings/options of IDMan, however the detailed lists of tweaks required (*before first run - recommended*) are as follows:

1. Options > General Tab > Customize IDM Download Panels in Browsers
   - `for web-players`: keep as is (TODO/?)
   - `for selected files`: change the panel view and selection option to:

![selected_files](./assets/selected_files.png)

2. Options > File Types:
   - Edit "automatically start downloading the following file types" to:
     - `extensions` are case insensitive, thus 'exe' and 'EXE' are the same thing.
     - seperate each extension by a ` ` (space) charecter.
     - extensions are *ordered* alphabetically (optional) for easier search.
     - wildcard charecter `*` is supported.

     ```
     3GP GZIP IMG ISO MP4 MKV MOV
     ```

   - Edit "don't start downloading automatically from the following sites" to:

     ```
     *.update.microsoft.com download.windowsupdate.com *.download.windowsupdate.com siteseal.thawte.com ecom.cimetz.com *.voice2page.com
     ```

3. Options to change file destination is available under `options > save to` tab.

</div>
