<div align="center">

<h1 align = "center">git Setup and Configuration</h1>
</div>

TODO Documentation [Help-Link](https://www.youtube.com/watch?v=lLgWWtOk7gk&ab_channel=sagarjunnarkar)

1. Add and edit **config** file in `~/.ssh`
2. Add and edit `~/.gitconfig`
3. In `.gitconfig` you can specify directories which you want to designate for specific works, for which include `[includeIf "gitdir:~/directory/"]` command
4. Set necessary details, and update each folder with their specific `.gitconfig` file as included here!
   - In `.gitconfig` the parameter `git-global-user.name` is **user1** as per *config*
   - Likewise, the email and user name for other directory is specified
