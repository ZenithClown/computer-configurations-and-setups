<div align="center">

# ⚙️ Program Settings

**TODO:** Autocomplete file, and copy settings for `windows` and `*nix`. Currently, *follow detailed instructions to setup the environment quickly* for each programs/applications.

**Direct Download Link(s):** 
| [KeePassXC](https://keepassxc.org/)
| [NVIDIA Drivers](https://www.nvidia.com/Download/index.aspx)
| [PuTTY](https://www.putty.org/)
| [WinSCP](https://winscp.net/eng/download.php)
| [GIT SCM](https://git-scm.com/downloads)
| [Docker Desktop](https://www.docker.com/products/docker-desktop/) <sup>?2</sup>
| [Microsoft Office](https://account.microsoft.com/services) <sup>?1</sup>
| [7zip](https://www.7-zip.org/download.html)
|

</div>

<div align = "justify">

## Start Menu

```
Computer Essentials 💻
Programs & Coding 🧮🖱️
  📔 Documents & Text Processing
  🔰♾️💻 🇵🇾🇹🇭🇴🇳 🇩🇪🇻 🇭🇴🇺🇸🇪
  🗃 Database Servers
👥🎼 Social Media & Entertainment
```

---

**footnotes**

  * <b>?1</b> : Office Home & Student 2019 Licensed Version (Laptop & Devices Login Microsoft A/C)
  * <b>?2</b> : Install <a href = "https://learn.microsoft.com/en-us/windows/wsl/install">WSL-2</a> by <b><code>wsl --install</code></b> from Windows PowerShell, followed by <a href = "https://docs.docker.com/desktop/install/windows-install/">docker</a> installation.

</div>
