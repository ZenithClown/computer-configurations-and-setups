# bash aliases to make life easier!
# the file is included in ~/.zshrc or ~/.bashrc to be recognized
# https://gitlab.com/ZenithClown/computer-configurations-and-setups/-/tree/master/Mint-20#bashrc-configurations

notebook() {
  # opens jupyter notebook in the current directory
  # NOTE: to be used only when conda notebook is configured without `conda init`
  # meaning, the default python3 is recognised first by the terminal
  # as the installation suggest, you have to first activate conda's base environment
  # ! change the directory/anaconda3 path where required
  eval "$(/home/debmalya/anaconda3/bin/conda shell.zsh hook)"
  # if you're using a different shell - replace `zsh` with the proper one
  # once done, you can open the notebook using the good old command:
  jupyter notebook
}

conda-activate() {
  # activates conda base environment when anaconda is not configured using `conda-init`
  # if you're using a different shell - replace `zsh` with the proper one
  # ! change the directory/anaconda3 path where required
  eval "$(/home/debmalya/anaconda3/bin/conda shell.zsh hook)"
}

system-upgrade() {
  # update all the available packages and
  # also upgrades the packages after confirmation
  sudo apt update
  sudo apt upgrade
}

apt-clean() {
  # remove optional packages and cache from apt
  # https://askubuntu.com/a/172516/521338
  sudo apt autoremove
  sudo apt autoclean
}

hide-windows-file() {
  # the command hides specific files that are generated (by default) in a windows
  # in linux, a file/directory can be made 'hidden' if they are included in a special file `.hidden`
  echo "Autorun.inf" >> .hidden
  echo "\$RECYCLE.BIN" >> .hidden
  echo "System Volume Information" >> .hidden
  # windows operating system also generates specific files that are binary - and not recongnised by *nix
  # all these files are added into `.hidden`
  ls | grep -E ".ico|.ini" >> .hidden
}

# git aliases are now configured using `.gitconfig` files
# thus previously used commands are now replaced using aliases
# use the `git` alias `g` followed by aliases defined under `.gitconfig/[alias]`
# example: initialize a directory with `g i`
alias g='git'

# list of other command aliases are defined below
alias ls='ls -lah --color=auto'
alias ll='ls -alF --color=auto'
alias os='lsb_release -a'

# create aliases for linux and windows homonyms (same function, different spelling)
alias md='mkdir'
alias cls='clear'
