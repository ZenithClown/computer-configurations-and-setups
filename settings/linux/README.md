<div align="center">

<h1 align = "center">Linux Mint 20 "Ulyana"</h1>
<b>Configuration & Setup</b> <br><br>

</div>

<p align = "justify"><b>NOTE:</b> this should work in all <i>debian-based</i> Systems!</b></p>

<p align = "justify">By default, like all distros <code>bash</code> si the defacto standard and default terminal! Some cool aliases and other configurations is included in <code>.bash_aliases</code> and <code>.bashrc</code> files respectively. List of custom bash aliases and their informations are as below:</p>

- **HIDE_WIN_FILE()** If you have dual boot system, like mine, or you have a USB Drive which is frequently used in Windows-OS then you will notice some file and/or folders are created by default. Hide these files by adding the names of the files in `.hidden` file.
- There are also some other aliases included in the file, which are self-explanatory!

**IMPORTANT NOTE:** Some new lines are only included into the `.bashrc` file and **only** append these lines into the file, and DO NOT REPLACE THIS FILE with the one already existing in your system.

## .bashrc Configurations

Enable `.bash_aliases` file options in `.bashrc` if uncommented or not included, with the following below lines. Now, all your aliases are automatically registered.

```bash
# Alias definitions.
# You may want to put all your additions into a separate file like
# ~/.bash_aliases, instead of adding them here directly.
# See /usr/share/doc/bash-doc/examples in the bash-doc package.

if [ -f ~/.bash_aliases ]; then
    . ~/.bash_aliases
fi
```

If you have ruby-gems installed then you might want to add the following lines. This is a powerful and useful utility, if you want to publish any website with GitHub Pages and/or GitLab Pages.

```bash
# Install Ruby Gems to ~/gems
export GEM_HOME="$HOME/gems"
export PATH="$HOME/gems/bin:$PATH"
```

With the below Lines, colored prompt is enabled, and also custom colors related to GIT and its repository is included.

```bash
# set a fancy prompt (non-color, unless we know we "want" color)
case "$TERM" in
    xterm-color|*-256color) color_prompt=yes;;
esac

# uncomment for a colored prompt, if the terminal has the capability; turned
# off by default to not distract the user: the focus in a terminal window
# should be on the output of commands, not on the prompt
force_color_prompt=yes

if [ -n "$force_color_prompt" ]; then
    if [ -x /usr/bin/tput ] && tput setaf 1 >&/dev/null; then
    # We have color support; assume it's compliant with Ecma-48
    # (ISO/IEC-6429). (Lack of such support is extremely rare, and such
    # a case would tend to support setf rather than setaf.)
    color_prompt=yes
    else
    color_prompt=
    fi
fi

if [ "$color_prompt" = yes ]; then
    PS1='${debian_chroot:+($debian_chroot)}\[\033[01;32m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]\$ '
else
    PS1='${debian_chroot:+($debian_chroot)}\u@\h:\w\$ '
fi
# unset color_prompt force_color_prompt

# Add git branch if its present to PS1
parse_git_branch() {
 git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/(\1)/'
}
if [ "$color_prompt" = yes ]; then
 PS1='${debian_chroot:+($debian_chroot)}\[\033[01;32m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\w\[\033[01;31m\]$(parse_git_branch)\[\033[00m\]\$ '
else
 PS1='${debian_chroot:+($debian_chroot)}\u@\h:\w$(parse_git_branch)\$ '
fi

# Changing the below Line Deleted the Color from the Prompt all-together.
# unset color_prompt force_color_prompt

# If this is an xterm set the title to user@host:dir
case "$TERM" in
xterm*|rxvt*)
    PS1="\[\e]0;${debian_chroot:+($debian_chroot)}\u@\h: \w\a\]$PS1"
    ;;
*)
    ;;
esac

# enable color support of ls and also add handy aliases
if [ -x /usr/bin/dircolors ]; then
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
    alias ls='ls --color=auto'
    #alias dir='dir --color=auto'
    #alias vdir='vdir --color=auto'

    alias grep='grep --color=auto'
    alias fgrep='fgrep --color=auto'
    alias egrep='egrep --color=auto'
fi

# colored GCC warnings and errors
#export GCC_COLORS='error=01;31:warning=01;35:note=01;36:caret=01;32:locus=01:quote=01'
```

## Z-Shell (zsh)

<p align = "justify">The Z-Shell (Zsh) is a Unix shell that can be used as an interactive login shell and as a command interpreter for shell scripting. Zsh is an extended Bourne shell with many improvements, including some features of Bash, ksh, and tcsh. Now, I've configures zsh along with <code>powerlevel10k</code> theme, with the configurations listed in <code>.zshrc</code> file. Complete installation is mentioned below, with additional helps and remarks. <b>Note</b> that the setup requires <code>git</code> installed in the system, you can install the same using <code>sudo apt install git</code>.</p>

```bash
sudo apt install -y zsh # Install zsh
sudo apt-get install -y powerline fonts-powerline

# `sudo` is not required for the following installations
# clone zsh into the given folder, or to the one of your choice
git clone https://github.com/robbyrussell/oh-my-zsh.git ~/.oh-my-zsh
cp ~/.oh-my-zsh/templates/zshrc.zsh-template ~/.zshrc

# Note, in the .zshrc file change according to your theme-preference
# this change is to be done in the variable `ZSH_THEME` as given below

# if you prefer powerlevel9k
ZSH_THEME="powerlevel9k/powerlevel9k"
git clone https://github.com/bhilburn/powerlevel9k.git ~/.oh-my-zsh/custom/themes/powerlevel9k

# else, if you prefer powerlevel10k
ZSH_THEME="powerlevel10k/powerlevel10k"
git clone https://github.com/romkatv/powerlevel10k.git ~/.oh-my-zsh/themes/powerlevel10k

# >>> DO THESE only if you prefer powerlevel10k >>>
# Enable auto-suggestions and auto complete
git clone https://github.com/zsh-users/zsh-autosuggestions.git ~/.oh-my-zsh/plugins/zsh-autosuggestions
git clone https://github.com/zsh-users/zsh-syntax-highlighting.git ~/.oh-my-zsh/plugins/zsh-syntax-highlighting

# Now in .zshrc replace plugins=(git) with the following:
plugins=(git zsh-autosuggestions zsh-syntax-highlighting)

# CLI Configure p10 - if not auto-triggered using:
p10k configure
# <<< DO THESE only if you prefer powerlevel10k <<<

# Change your DEFAULT Shell to zsh
chsh -s /bin/zsh # Restart the system and you are done
```

<p align = "justify">Review <a href = "https://medium.com/@thecaffeinedev/customize-your-terminal-oh-my-zsh-on-ubuntu-18-04-lts-a9b11b63f2">step-by-step guide</a> with powerlevel9k and <a href = "https://medium.com/@shivam1/make-your-terminal-beautiful-and-fast-with-zsh-shell-and-powerlevel10k-6484461c6efb">powerlevel10k</a> for more information and other configurations! On succesful completion check <code>echo $ZSH_CUSTOM</code> which should point to <code>~/.oh-my-zsh/custom</code> if you have installed <i>oh-my-zsh</i> with the folder settings as mentioned above.</p>

<p align = "justify">If you use anaconda distribution, then you might want to show the conda environment and all other functionalities. For that, initialize conda with <code>/(your conda installation path)/bin/conda init zsh</code> which should add the following lines in your <code>.zshrc</code>:</p>

```bash
# >>> conda initialize >>>
# !! Contents within this block are managed by 'conda init' !!
__conda_setup="$('/(your conda installation path)/bin/conda' 'shell.zsh' 'hook' 2> /dev/null)"
if [ $? -eq 0 ]; then
    eval "$__conda_setup"
else
    if [ -f "/(your conda installation path)/etc/profile.d/conda.sh" ]; then
        . "/(your conda installation path)/etc/profile.d/conda.sh"
    else
        export PATH="/(your conda installation path)/bin:$PATH"
    fi
fi
unset __conda_setup
# <<< conda initialize <<<
```

<p align = "justify"><b>NOTE:</b> all the bash_aliases are recognized by .zsh, but you have to include the following lines into the <code>.zshrc</code> if not included (the file present in this directory has the lines included).</p>

```bash
# Alias definitions. Same as that in .bashrc
# You may want to put all your additions into a separate file like
# ~/.bash_aliases, instead of adding them here directly.
# See /usr/share/doc/bash-doc/examples in the bash-doc package.

if [ -f ~/.bash_aliases ]; then
    . ~/.bash_aliases
fi
```

## Useful Aliases
Some useful alias, that can be set in either `.bashrc` or `.zshrc` or other shells, are mentioned below:

```bash
alias ping='ping -c 4' # https://askubuntu.com/a/200992/521338
```
