<div align = "justify">

This repository is developed for own personal usage. Unless specifically mentioned [issues](https://gitlab.com/ZenithClown/computer-configurations-and-setups/-/issues) with a label `help-wanted` is mentioned, any other PR will be automatically rejected. If you want to use the repository/any of its content, then please [fork](https://gitlab.com/ZenithClown/computer-configurations-and-setups/-/forks/new) the repository and make the required changes.

Only the following [issues](https://gitlab.com/ZenithClown/computer-configurations-and-setups/-/issues/?sort=created_date&state=opened&label_name%5B%5D=helpwanted&first_page_size=20) are currently accepting PR request. Please check the issue description for more information.

</div>
