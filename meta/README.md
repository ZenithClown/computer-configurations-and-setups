<div align="center">

<h1 align = "center">Auto-Configuration and Setup</h1>

</div>

<div align = "justify">

🤓 Being a tech-enthusiast 💻, I spent much time and efforts in polishing ⚙️ my own working environment to perfection! Well, with experience 🧑 and maturity many settings and configurations 🛠️ are revamped and modified. In case of system-crash 🤞 the `meta` directory will *quickly setup* 🔗 folder and directories to start working.

## Getting Started

Create a 📁 directory with additional configurations using the ⚙️ `config.json` file, as structured below. Finally, start the 🛠️ setup using `python setup.py kwargs` to create directories, set icons and/or clone optional code bases.

```yaml
{
    "<folder-name>" : {
        "icon" : [<directory>, <to>, <icon>, <file>], # used as `os.path.join()`
        "git-dirs" : <bool> # specify if clone from github/gitlab
        "repositories" : [
            # if git-dirs == True, then clone each directory from this list
            # into the parent "<folder-name>" using the default credentials configured with `.gitconfig`
            {
                "uri" : "URL to clone from, can use `{token}` from Environment/Path Variables",
                "dst" : "Directory Name, if to change after cloning, else keep `null` to keep the default",
                # set an icon file which MUST be present in the repository, and uses the same convention
                "ico" : [<directory>, <to>, <icon>, <file>], # used as `os.path.join()`
            }
        ]
    }
}
```

Development environment is always great is `linux` systems 💘! So, I also use *linux directory* convention to save files and system configurations. Basic directories that I use are mentioned below, for a detailed explanations check [this link](https://linuxhandbook.com/linux-directory-structure/).

1. `/lib` : Shared Libraries, use this to save frequently used codes/files that needs to be accessed globally.
   * `/lib/mssql` : Save MS SQL Code Files
   * `/lib/python` : Save `python` modules. `PYTHONPATH` points to this directory.
2. `/tmp` : Temporary files and directories.
3. `/usr/bin` : Binary files, for example install `python 3.9` globally while keep binary file for other *python version* in this directory.
4. `/var` : Variable data files, logs, and template files that changes frequently.

</div>
