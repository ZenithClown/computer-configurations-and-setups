@ECHO OFF

@REM ! the code expects a directory name as
@REM * the root directory at positional argument `[0]`
SET ROOT=%1

@REM ! in addition, provide the icon file name
@REM ! the default icon name should be controlled externally
@REM * expects the folder icon name at `[1]`
SET ICON=%2

@REM write contents and set directory attributes
(
    ECHO [.ShellClassInfo]
    ECHO IconResource=".\%ICON%",0
) > "%ROOT%\desktop.ini"

attrib +s +h -a "%ROOT%\desktop.ini"
attrib +r "%ROOT%"
attrib +s +h "%ROOT%\%ICON%"
