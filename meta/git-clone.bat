@ECHO OFF

@REM ! the code expects url to clone from
@REM ! send the key with access token/keys if required
SET URI=%1

@REM the code is modified to populate a specific directory
@REM this uses the extended git clone command to clone into directory
SET DIR=%2

@REM * clone the directory, and exit
git clone %URI% %DIR%
