# -*- encoding: utf-8 -*-

"""
This initializes an empty drive for use. I prefer to keep codes, and
files in some particular fashion and this code initializes them for
the same. The following directory structure is used:
  * `/var/` : variables directory, whose content is expected to
              change continuously.
              > `/var/logs/` : for keeping code logs.
  * `/temp/` : temporary files, directories. can be automated to
               delete content on a periodic basis.
"""

import os
import json
import time
import shutil
import argparse
import warnings

makedir  = lambda path : os.makedirs(path, exist_ok = True)
setIcon  = lambda dir, icon : os.system(f"set-folder-icon.bat {dir} {icon}")
gitclone = lambda uri, dest : os.system(f"git-clone.bat {uri} {dest}")

if __name__ == "__main__":
    parser = argparse.ArgumentParser(__doc__) # command line named arguments

    # add the command line named arguments that are
    # * either positional based, parsed with `sys.argv`, or
    # * named basis parsed using the `parser` object

    parser.add_argument(
        "-d", "--drive",
        default = ".", dest = "ROOT",
        help = "Drive Letter (like `C:`), Defaults to Current Directory"
    )

    parser.add_argument(
        "--git-token",
        default = None, dest = "GIT_AUTH_TOKEN",
        help = "Clone `git` Repositories with a Authentication Token"
    )

    parser.add_argument(
        "--additional-directory",
        default = False, dest = "DIRS",
        help = "Comma Seperated List of Additional Directories"
    )

    parser.add_argument(
        # * accepted arguments (`Y` / `N`) - any case
        # ! let user decide if they want to create all directories
        # ! set `y` to override and create all directory
        "--create-all",
        default = "N", dest = "ALL",
        help = "Create All Directories? (`Y/y` for 'yes', any other key for 'no')."
    )

    parser.add_argument(
        "--create-organizations",
        default = False, dest = "ORGS",
        help = "Comma Seperated List of Additional Directories"
    )

    # get the arguments from parser
    # * using `argparse` : https://stackoverflow.com/a/40002420/6623589
    # * using named arguments : https://stackoverflow.com/a/47389858/6623589
    # ! setting required arguments : https://stackoverflow.com/a/24181138/6623589
    # * using `sys.argv` and `argparse` : https://stackoverflow.com/a/29994228/6623589
    args = parser.parse_args()

    # check accepted arguments, else set to check individual folder
    if args.ALL.upper() == "Y":
        ALL_DIR = True
    else:
        ALL_DIR = False

    # get icons file informations from config
    config = json.load(open("config.json", "r"))
    
    ### --- start creating directories --- ###
    DIRECTORIES = ["tmp", "tmp/.torrents", "var", "var/logs", "var/templates", "lib", "lib/python", "lib/mssql", "usr", "usr/bin"]

    if args.DIRS:
        DIRECTORIES += args.DIRS.split()

    if args.ORGS:
        structure = {
            # ! key should be the base level folder name
            # * for any additional directory, add like `key, vals`
            "IIT-ISM" : [], # no additional directory yet
            "pOrgz-dev" : [],
            "iWorks" : ["tmp", "var", "var/logs", "var/tests", "lib", "lib/python", "lib/mssql", "docs", "notebooks", "projects"],
            "Reliance JIO" : ["tmp", "var", "var/templates", "gitlab", "gitlab/SC RMS", "gitlab/algorithms", "gitlab/reporting"]
        }
        for org, subdirs in structure.items():
            prompt = input(f"Create <ORG:{org}> (Y/n)?").strip().upper()
            if prompt == "Y":
                DIRECTORIES += [org] # append to root directory
                DIRECTORIES += [os.path.join(org, subdir) for subdir in subdirs]

    print(time.ctime(), f" Creating Directories at {args.ROOT}")
    for p in DIRECTORIES:
        src = os.path.join(args.ROOT, p)
        print(f" Resolved Directory Path: {src}")

        # * let user choose if they want to create the folder, if
        # `args.ALL` is set to `False`
        if not ALL_DIR:
            prompt = input(f" > Create Resolved Directory (Y/n)? ").strip().upper()
            prompt = True if prompt == "Y" else False
        else:
            prompt = True

        if prompt:
            makedir(path = src)

            # ! copy the icon file, and paste it as `favicon.ico`
            # * in the newly created dictionary
            try:
                shutil.copy(
                    os.path.join(".", "icons", *config.get(p)["icon"]),
                    os.path.join(src, "favicon.ico")
                )
            except (KeyError, TypeError):
                warnings.warn(f"Icon File Not Defined for {p}")
            except PermissionError as err:
                warnings.warn(f"Not Processed for `{err}`")

            # ! set the folder icon using the existing batch script
            setIcon(dir = src, icon = "favicon.ico")

            try:
                # ! check if there is any additional git directories for the current folder
                # * if true, then check the directories, and clone them
                # if a git directory has a specific icon, get the same from within the repository
                if config.get(p)["git-dirs"]:
                    for repo in config.get(p)["repositories"]:
                        dir_name = repo["uri"].split("/")[-1].replace(".git", "")
                        gitclone(
                            # ..versionadded: eacfb67587486e5f3a6cea751b9fc28c875a0a98
                            # ..usage: --git-token="<github-username>"
                            # TODO setup `GITHUB_TOKEN` and `GITLAB_TOKEN` and parameterize
                            uri = repo["uri"].format(GITHUB_TOKEN = f"{args.GIT_AUTH_TOKEN}:{os.getenv('GITHUB_TOKEN')}@"),
                            dest = os.path.join(src, dir_name)
                        )
                        if repo["ico"]:
                            setIcon(dir = os.path.join(src, dir_name), icon = os.path.join(*repo["ico"]))

                        if repo["dst"]:
                            # ..versionadded: 6a86fbaff495b082a5fce4863c771e4aeb087ad2
                            # let config file keep the same name as that of the repository, set the
                            # `dst` parameter as `null` in `config.json` else, keep any string valued name
                            shutil.move(os.path.join(src, dir_name), os.path.join(src, repo["dst"]))
            except Exception as err:
                warnings.warn(f"Failed to Clone : {err}")

            print(" >> resolved/created.")
        else:
            print(" >> skipped.")
    
    # restart explorer, as required to restart
    os.system("restart-explorer.bat")
