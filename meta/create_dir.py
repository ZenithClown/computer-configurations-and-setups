# -*- encoding: utf-8 -*-

"""
Create an Individual Directory on a Specified Path Location

Simply provide the `--location` parameter, defaults to current
directory, and the icon file path `--icopath` and the folder is
created. Example:

```shell
python create_dir.py --location="E:\\dev-works" --icopath="icons,generic,dev-ops.ico"
```
"""

import os
import shutil
import argparse

from setup import makedir, setIcon

if __name__ == "__main__":
    parser = argparse.ArgumentParser(__doc__) # command line named arguments

    # follows most given conventions as in `setup.py`
    parser.add_argument(
        "--location",
        default = ".", dest = "ROOT",
        help = "Drive Letter (like `C:\\`), Defaults to Current Directory"
    )

    parser.add_argument(
        "--icopath",
        default = None, dest = "ICON",
        help = "ICON File Location on the `./icons` Directory, in CSV Format"
    )

    # get the arguments from parser
    args = parser.parse_args()

    # create directory on the mentioned folder
    # ! no need of `os.path.join` format expects the same from user
    makedir(path = args.ROOT)

    if args.ICON:
        __icon_path__ = os.path.join(*args.ICON.split(","))

        # try to set folder ICON, as in `setup.py`
        shutil.copy(__icon_path__, os.path.join(args.ROOT, "favicon.ico"))
        setIcon(dir = args.ROOT, icon = "favicon.ico")

        # restart explorer, as required to restart
        # * no need to restart if icon is not mentioned
        os.system("restart-explorer.bat")
