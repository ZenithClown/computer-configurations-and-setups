<div align = "justify">

Directory to configure a Linux-based system. Generally, I prefer [Linux Mint](https://linuxmint.com/) which is a fork of
[Ubuntu](https://ubuntu.com/) and uses the `apt` package manager by default. The setup files can be used to quickly set up
the system and clone any additional directories or files required for the development environment. Firstly, let's configure
and install `git` which can be used to clone any additional repositories and start the setup with ease.

```shell
sudo apt upgrade && sudo apt upgrade; # install upgrades on clean system

# install `git` using `apt` package manager, and configure globals
sudo apt install -y git

git config --global user.name ZenithClown
git config --global user.email "debmalyapramanik.005@gmail.com"

# it is better to use github/gitlab in linux with ssh-keys
# more information - https://docs.gitlab.com/ee/user/ssh.html
# the following is an ssh-clone key command, else use https clone link like:
# git clone https://gitlab.com/ZenithClown/computer-configurations-and-setups.git config

git clone git@gitlab.com:ZenithClown/computer-configurations-and-setups.git config
gio set -t 'string' ~/config 'metadata::custom-icon' 'file:///~/config/meta/icons/generic/images/gear.png'
```

</div>
