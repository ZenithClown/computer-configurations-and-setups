#####################################################################
#                                                                   #
#           ZSH Setup on DEBIAN with APT Package Manager            #
#                                                                   #
#                                                                   #
# Generally, I prefer `zsh` with `powerlevel10k/powerlevel10k`      #
# theme, which provides easy to use interface with auto-suggestions #
# for development environment.                                      #
# More details on setup is available under /settings/linux.         #
#                                                                   #
#                                                                   #
# Author: Debmalya Pramanik                                         #
# Contact: https://zenithclown.github.io/contacts.html              #
#                                                                   #
#####################################################################

sudo apt upgrade && sudo apt upgrade # install upgrades on clean system
cd ~ # under users home, install and configure `zsh`

# clone zshell directory and configure everything
sudo apt install -y zsh # install zshell
sudo apt-get install -y powerline fonts-powerline # fonts and additional package

# clone `Oh-My-ZSH` Community Driven Framework for Managing ZSH
git clone https://github.com/robbyrussell/oh-my-zsh.git ~/.oh-my-zsh
cp ~/.oh-my-zsh/templates/zshrc.zsh-template ~/.zshrc

# additionally get themes and plugins for `zsh`
git clone https://github.com/romkatv/powerlevel10k.git ~/.oh-my-zsh/themes/powerlevel10k
git clone https://github.com/zsh-users/zsh-autosuggestions.git ~/.oh-my-zsh/plugins/zsh-autosuggestions
git clone https://github.com/zsh-users/zsh-syntax-highlighting.git ~/.oh-my-zsh/plugins/zsh-syntax-highlighting

##### >>> MANUAL STEPS <<< #####
echo Edit the "~/.zshrc" file for "ZSH_THEME" and "plugins"
echo Check the ~/config/setup/linux/zsh-setup.sh for more information.
# ZSH_THEME="powerlevel10k/powerlevel10k"
# plugins=(git zsh-autosuggestions zsh-syntax-highlighting)

# once edit is complete, we can change the default shell
# and restart the system to start configuring the zsh with `p10k` which should start automatically
# chsh -s /bin/zsh
# echo "Restart the System `sudo shutdown -hr now` and Open Terminal for `p10k` Configuration"
