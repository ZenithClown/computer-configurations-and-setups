<h1 align = "center">MySQL Local Database</h1>

<div align = "justify">

Local database can be installed via the `docker` environment, and the same can be connected through MySQL Workbench without installing MySQL Client in Local Machine. [Video Tutorial](https://www.youtube.com/watch?v=X8W5Xq9e2Os). First, docker needs to be configured, and MySQL Workbench can be downloaded from [here](https://dev.mysql.com/downloads/workbench/). Create an image, and login into the *bash terminal* and login to MySQL client from shell as:

```shell
~ docker run --name "image-name" -p 33060:3306 -e MYSQL_ROOT_PASSWORD=root -d mysql:latest
~ docker exec -it pOrgz-dev /bin/bash
bash# mysql -u root -p -A
```
So now you are logged into the MySQL Client from the docker environment. To login to `root` user from localhost, you have to change the privileges of the `root` user as:

```sql
mysql> SELECT user, host FROM mysql.user;

+------------------+-----------+
| user             | host      |
+------------------+-----------+
| mysql.infoschema | localhost |
| mysql.session    | localhost |
| mysql.sys        | localhost |
| root             | localhost |
+------------------+-----------+
5 rows in set (0.00 sec)

mysql> UPDATE mysql.user SET host = "%" WHERE user = "root";
mysql> FLUSH PRIVILEGES;
mysql> exit
```

Ensure that `root` user does not have a duplicate entry for both `localhost` and `%` entry. In this case, you may want to delete the record and then flush privileges as:

```sql
mysql> DELETE FROM mysql.user WHERE user = "root" and host = "localhost";
mysql> FLUSH PRIVILEGES;
mysql> exit
```

Now you can login using MySQL Workbench and view all tables and do some queries on the database.

</div>
