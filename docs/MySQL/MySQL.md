# MySQL
Follow the process to install *MySQL: Server, Client* and *Workbench* in Debain based system:
```bash
d@dPramanik:~$ sudo apt-get install mysql-server # Downloads and installs clients and server
d@dPramanik:~$ mysql --version # Check the installed version
mysql  Ver 8.0.21-0ubuntu0.20.04.4 for Linux on x86_64 ((Ubuntu))
d@dPramanik:~$ sudo mysql_secure_installation # Configure MySQL for first time use, and enable security
```

Secure installation using `mysql_secure_installation` provides various features that has to be configured, which is as follows:
```
Securing the MySQL server deployment.

Connecting to MySQL using a blank password.

VALIDATE PASSWORD COMPONENT can be used to test passwords
and improve security. It checks the strength of password
and allows the users to set only those passwords which are
secure enough. Would you like to setup VALIDATE PASSWORD component?

Press y|Y for Yes, any other key for No: y

There are three levels of password validation policy:

LOW    Length >= 8
MEDIUM Length >= 8, numeric, mixed case, and special characters
STRONG Length >= 8, numeric, mixed case, special characters and dictionary file

Please enter 0 = LOW, 1 = MEDIUM and 2 = STRONG: 1
Please set the password for root here.

New password: 

Re-enter new password: 

Estimated strength of the password: 100 
Do you wish to continue with the password provided?(Press y|Y for Yes, any other key for No) : y
By default, a MySQL installation has an anonymous user,
allowing anyone to log into MySQL without having to have
a user account created for them. This is intended only for
testing, and to make the installation go a bit smoother.
You should remove them before moving into a production
environment.

Remove anonymous users? (Press y|Y for Yes, any other key for No) : y
Success.


Normally, root should only be allowed to connect from
'localhost'. This ensures that someone cannot guess at
the root password from the network.

Disallow root login remotely? (Press y|Y for Yes, any other key for No) : y
Success.

By default, MySQL comes with a database named 'test' that
anyone can access. This is also intended only for testing,
and should be removed before moving into a production
environment.


Remove test database and access to it? (Press y|Y for Yes, any other key for No) : y
 - Dropping test database...
Success.

 - Removing privileges on test database...
Success.

Reloading the privilege tables will ensure that all changes
made so far will take effect immediately.

Reload privilege tables now? (Press y|Y for Yes, any other key for No) : y
Success.

All done!
```

**Note:** Pressing `y` for `Disallow root login remotely?` does not allow root login from users, except root. Open MySQL using *sudo* and you can create some other users and grant privilleges to different users:
```sql
d@dPramanik:~$ sudo mysql -u root
Welcome to the MySQL monitor.  Commands end with ; or \g.
Your MySQL connection id is 15
Server version: 8.0.21-0ubuntu0.20.04.4 (Ubuntu)

Copyright (c) 2000, 2020, Oracle and/or its affiliates. All rights reserved.

Oracle is a registered trademark of Oracle Corporation and/or its
affiliates. Other names may be trademarks of their respective
owners.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

mysql> CREATE USER '<user-name>'@'localhost' IDENTIFIED BY '<password-as-per-setup-policy>';
```

Creating a password must fullfil `setup-policies` as given while setting up, i.e. in this case `Please enter 0 = LOW, 1 = MEDIUM and 2 = STRONG: 1` thus all password must require `MEDIUM Length >= 8, numeric, mixed case, and special characters` configuration.
```sql
mysql> CREATE DATABASE '<database-name>'; # Create a database
mysql> GRANT ALL PRIVILEGES ON * . * TO '<user-name>'@'localhost'; # this gives root privilege to <user-name>
mysql> FLUSH PRIVILEGES; # Reload all privileges
```

However, in most cases we do not want to give root privileges to a user. Grant specific permission as follows:
```sql
mysql> GRANT ALL PRIVILEGES ON <database-name>.<table-name> TO '<user-name>'@'localhost'; # give all access to specific table in a database
mysql> FLUSH PRIVILEGES; # Reload all privileges
```

[More Information](https://www.digitalocean.com/community/tutorials/how-to-create-a-new-user-and-grant-permissions-in-mysql)

[Helpful Link](https://devanswers.co/cant-connect-mysql-server-remotely/)

## Installing MySQL WorkBench

