Rough Instructions, with Important Links are as Follows:
1. [apache2](https://linuxhostsupport.com/blog/how-to-host-a-website-on-vps/) Configurations
   - Create directory like _httpd_ and _domain1.com_ then restart/reload apache2
2. Deploy Flask APP using [this link](https://www.digitalocean.com/community/tutorials/how-to-deploy-a-flask-application-on-an-ubuntu-vps)


SSH Connection:
`ssh user@host` for connecting remotely from terminal, and for port forwarding, use `ssh -L SourceHost:PORT:DestinationHost:PORT user@host`!