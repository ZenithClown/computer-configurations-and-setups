# .NET Framework
A Guide to .NET Framework Configuration and Complete Instalation on a Debian based System (like Ubuntu). Follow the original link(s) for more information and debugs.
1. [.NET Installaion, by Round-the-Code](https://www.roundthecode.com/dotnet/asp-net-core-web-hosting/how-to-install-an-asp-net-core-in-net-5-app-on-ubuntu-20-04)
2. [SDK and Runtime Environment Installation, by Code-Grepper](https://www.codegrepper.com/code-examples/shell/install+.net+framework+5+on+ubuntu+20.04)

**Note:** skipping _apache_ configuration as it is a seperate topic.

```bash
wget https://packages.microsoft.com/config/ubuntu/20.04/packages-microsoft-prod.deb -O packages-microsoft-prod.deb
dpkg -i packages-microsoft-prod.deb

# install dependency
sudo apt-get update
sudo apt-get install -y apt-transport-https
sudo apt-get update

# install the SDK - for developing the application
sudo apt-get install -y dotnet-sdk-5.0

# installing the runtime
sudo apt-get install -y aspnetcore-runtime-5.0
```

Apart from this, you will need to write the **necessary service files** under `/etc/systemd/system/service-name.service`, a simple such service configuration file is like below.

```bash
[Unit]
Description = The description can be any text, helpful when checking journalctl logs

[Service]
WorkingDirectory = path/to/api/directory
ExecStart        =/usr/bin/dotnet path/to/api/directory/main.dll
Restart          = always
RestartSec       = 10
KillSignal       = SIGINT
SyslogIdentifier = dotnet-pmp
User             = root
Environment      = ASPNETCORE_ENVIRONMENT = Production 

[Install]
WantedBy = multi-user.target
```

Also, the **apache2** configuration might be set in `/etc/apache2/sites-enabled/application-name.conf` like:

```bash
<VirtualHost *:*>
    RequestHeader set "X-Forwarded-Proto" expr=%{REQUEST_SCHEME}
</VirtualHost>

<VirtualHost *:80>
    ProxyPreserveHost On
    ProxyPass         / http://127.0.0.1:5050/
    ProxyPassReverse  / http://127.0.0.1:5050/
    ServerAdmin       info@example.com
    ServerName        example.com
    ServerAlias       www.example.com
    DocumentRoot      path/to/api/directory
    LogLevel          warn
    ErrorLog          /var/log/httpd/example.com_error.log
    CustomLog         /var/log/httpd/example.com_access.log combined
</VirtualHost>
```
