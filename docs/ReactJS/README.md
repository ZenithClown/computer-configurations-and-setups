1. Install [NodeJS](https://nodejs.org/)
2. Good Tutorials:
    - [ReactJS Tutorial - Code with Harry (YouTube)](https://www.youtube.com/playlist?list=PLu0W_9lII9agx66oZnT6IyhcMIbUMNMdt)
    - [Build an Admin Panel Dashboard - Maruf Sarker (YouTube)](https://www.youtube.com/playlist?list=PLEYW3pZS6IQ_a-iYAno4VsZonrikphq8L)

Creating a ReactJS Application:

```shell
npx create-react-app my-app

cd my-app # go into the created folder
npm start # start a react application in browser
```

The **Node Package Execute (`npx`)** focuses on installing/building components w/o the need of installing a package using the **Node Package Manager (`npm`)** [[reference](https://youtu.be/hnVOvvbQrwA?list=PLu0W_9lII9agx66oZnT6IyhcMIbUMNMdt&t=158)].
