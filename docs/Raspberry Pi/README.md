<div align="center">

<h1 align = "center">Raspberry Pi Configuration</h1>
<b>Model-B | RPi 3+</b>

</div>

<p align="justify">Generally, all the settings and commands should work on all <i>debian</i> based system, with <i>ARM</i> architecture. The system has the following:</p>

## Samba

Samba file sharing can be enabled, [more details](https://wiki.learnlinux.tv/index.php/Setting_up_Simple_Samba_File_Shares) or you can also check the [YouTube Video](https://www.youtube.com/watch?v=7Q0mnAT1MRg). The configuration files of the directory `/etc/samba/` are available.

## LAMP Server

MySQL is not available by default in Raspberry Pi [questionaire](https://raspberrypi.stackexchange.com/questions/73000), however equivalent **MariaDB** can be installed. Details are available in [this video](https://www.youtube.com/watch?v=8FyqyO-k16M). All the necessary Commands are here as follows:

```bash
sudo apt update
```

### Apache Server

```bash
sudo apt install apache2
```

Check apache is running by visiting `localhost`. If using UFW and SSH, then LIMIT PORT 80 with `sudo ufw limit 80`.

### Database : MariaDB

```bash
sudo apt install mariadb-server-10.0
```

All the `mysql` terminal commands will work with MariaDB as it is a fork of MySQL. Thus, proceed with Secure Installation using:

```bash
sudo mysql_secure_installation
```

Check the MySQL Installation for more information. Just set root user password for MariaDB and set `yes` to other options!

### Setting up the Web-Server
The web-server can be set up at `/var/www/html/` directory, and you can change it according to your need!