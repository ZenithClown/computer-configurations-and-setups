Google Mails (GMail) : Process of Downloading *some emails* with attachments in an `MBox` format. Use the `mbox-viewer` application by SysTools to view the contents of the file.

1. GOTO : [takeout](https://takeout.google.com/) and `SELECT Mail`.
2. Select the required emails (recommended to download only one `label`).
3. GOTO : `Next Step` and choose download format.
4. Download the file from the `email link` received within 7 days.

[GMail MBOX Viewer](https://www.emaildoctor.org/blog/free-gmail-mbox-viewer/) - good client, [virustotal](https://www.virustotal.com/gui/file/430542d48e86ad9bb1317519c11f91202e3ac9dd959f78a43a6410a50ebbcb14) report.

[Documentation on Google Takeout](https://www.indeed.com/career-advice/career-development/download-emails-from-gmail)
