<div align = "justify">

# Docker

**Docker** is a tool for running applications in an isolated environment. The platform designed to help developers build, share, and run modern applications.

## Getting Started

Installing `docker` in windows is straightforward. First, install [WSL-2](https://learn.microsoft.com/en-us/windows/wsl/install) followed by [docker desktop](https://docs.docker.com/desktop/install/windows-install/) installation. Do check the [Windows Prerequistes](https://gitlab.com/ZenithClown/computer-configurations-and-setups#prequistes) for more information.

### Docker Desktop in Linux

[Docker Desktop](https://www.docker.com/products/docker-desktop/) is now supported on Linux. To do this, first install the dependencies as:

```zsh
# https://docs.docker.com/desktop/linux/install/
modprobe kvm # required kvm virtualization
sudo apt update
sudo apt install qemu-system-x86
sudo apt --fix-broken install # to fix broken installed packages
```

Once done installing the dependencies, install via the following commands. For more information on installing for Pop-OS is available [here](https://linuxhint.com/install-docker-on-pop_os/).

```zsh
$ sudo apt update
$ sudo apt install  ca-certificates  curl  gnupg  lsb-release
$ curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
$ echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
$ sudo apt update
$ sudo apt install docker-ce docker-ce-cli containerd.io -y

# finally check the installed version using:
$ docker --version
Docker version 20.10.16, build aa7e414

# install docker desktop
# first download necessary deb/rpm package from:
# https://www.docker.com/get-started/
# then install using `dpkg` manager as:
# if `pass` error is thrown, then install/fix dependencies using:
$ sudo apt --fix-broken install
$ sudo dpkg -i docker-desktop-4.8.1-amd64.deb # 4.8.1 is the current stable release version
```

### Verification of Docker Installation

```zsh
docker --version # check the version of docker installed

# docker-compose is a tool for running multi-container applications
docker-compose --version # check version

# get help from docker
docker --help # display all commands
docker COMMAND --help # display help for a particular command

# List All Containers
docker ps -a # a: for all

# stop containers
docker rm CONTAINER-NAME # to stop a particular container
docker rm $(docker ps -aq) # stop all containers, q: list ids of container name

# run a new container
docker run --name <CONTAINER-NAME-OPTIONAL> -d -p <HOST>:<SOURCE> CONTAINER-NAME:VERSION # d: detached mode

# build from docker-compose.yml
docker compose up -d # change file with --file
```

Run the below commands carefully!

```zsh
docker system prune -a # prune/purge all images and containers
docker volume rm $(docker volume ls -q) # remove all volumes which are not in use
```

---

**DISCLAIMER** I'm a 🎓 learning `docker` and `kubernetes` and the course content is avilable in [YouTube - Docker and Kubernetes Tutorial | Full Course [2021]](https://www.youtube.com/watch?v=bhBSlnQcq2k&t=3310s&ab_channel=Amigoscode) by Amigoscode and Tech with Nana. Other contents sources are mentioned as required.

</div>
