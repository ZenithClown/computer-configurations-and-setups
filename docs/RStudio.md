# RStudio

Installation of `R` and `R-Studio` is pretty simple forward. Considering **Debian 10 / Ubuntu 18 / Ubuntu 20**, this can be installed using the following commands:

```bash
sudo apt-get install r-base # install r-base : command line utility
sudo apt-get install gdebi-core
wget https://download2.rstudio.org/server/bionic/amd64/rstudio-server-1.3.1073-amd64.deb
sudo gdebi rstudio-server-1.3.1073-amd64.deb

```

[More Information](https://rstudio.com/products/rstudio/download-server/debian-ubuntu/)

## Configuring IPython Notebook
To use RStudio in conjunction with Anaconda, open `anaconda-navigator` and create an environment for `R` and install `RStudio`.
