Some essential LINUX Commands - for everyday usage!
| Command | Remarks/Information | Variants |
| --- | --- | --- |
| `ssh user@hostname` | Connect to a Remote Host <br> using SSH Connection. | - Use the `-L` option for port forwarding (i.e. tunnelling). [More Information](https://linuxize.com/post/how-to-setup-ssh-tunneling/#local-port-forwarding) <br> - Use `-X` option for using `xclip` commands. [More Information](https://stackoverflow.com/questions/18695934) |
| `ps -aux \| grep <PID>` | Check information on a Process ID (PID) <br> `grep` is optional, but useful for finding a particular process. | |
| `netstat -ltnp \| grep -w 'P'` | Port Listener, where `P` is the Port Number | |
| `nohup <commands> > log.txt 2>&1 &` | Run a process in background, using `nohup` | |
| `xclip -sel` | Copy output from Terminal to Clipboard. | `tail -n 25 LOG \| xclip -sel clip` |
| `scp -P 5522 local-file root@host:/path/filename` | Copy data server to server | [More Information](https://stackoverflow.com/questions/10341032/scp-with-port-number-specified) |

## SSH Connection
Setting up of SSH (Secure Shell) to connect/access Linix Machine from Windows Machine, which is on the same network. For this, first install and configure SSH on Linux System, and to check if it is up and running: `sudo systemctl status ssh`. **Note:** If you get an error `unit ssh.service could not be found`, then mostly SSH Server is not installed in your system ([Ref. Link](https://unix.stackexchange.com/questions/520341)). Now, follw the steps:

- Allow SSH Connection over Firewall. But, for this **do not allow** everything over SSH but issue the command `sudo ufw limit ssh`.
- Now, check your system IP address using `ifconfig` under `inet`.
- Download and install [PuTTY](https://www.putty.org/) for Windows.
- Connect to your linux machine using PuTTY.
- To transfer files, download [WinSCP](https://winscp.net/eng/download.php).

**Note:** By default, on SSH Connection, if some other terminal like the `ZSH` is configured on linux, it is ignored by the PuTTY-agent. For this, you will have to edit `.bashrc`. Check its configuration [here, Mint-20 Directory](https://gitlab.com/ZenithClown/computer-configurations-and-setups/-/tree/master/Mint-20). For [More Information and Explanations](https://linuxize.com/post/how-to-enable-ssh-on-ubuntu-18-04/) on SSH Connection.
