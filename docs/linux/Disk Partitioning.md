<p align = "justify">The document provides help links and/or guide on disk partitioning while installing linux on a system. The document is heavily derived from <a href = "https://askubuntu.com/questions/343268">Ask-Ubuntu Q343268</a> with some other additional informations and my personal setup guide.</p>

## System Configuration
<p align = "justify">Considering a SSD of 240 GB, the recommended partition size is mentioned below. My system configuration is as below (using <code>neofetch</code>).</p>


<p align = "justify"><b>Recommended Configuration</b> considering (i) hibernation facility, (ii) seperate mount point for <code>boot</code>, <code>var</code> etc., and (iii) considering additional storage space for personal files.</p>

| Size (MB) | Size (GB) |  | Mount Point | Partition Type | Remarks/Comments |
|:---:|:---:|---|:---:|:---:|---|
| 1024 | - | EFI System Partition |  |  | Most common standard is 100-550 MB ([ref.](https://askubuntu.com/a/1313158/521338)). A *free space* of 1 MB is often registered in the partition table, thus the actual size will be `549 MB`. |
| 16384 | 16 | `swap` |  | primary | For a physical memory (RAM) of $`\leq 8`$ GB and hibernation options, swap size should be twice the size of RAM. |
| 40960 | 40 | `ext4` | `/` | **logical** | **Root Partition** is to be set as `logical` and should follow `ext4` journaling file system. Infact, apart from `swap` all other should follow `ext4` system. |
| 2048 | 2 | `ext4` | `/boot` | primary | Recommended size is 300-500 MB, however due to upgraded system boot drive is taking much space and there is often a `low disk space` error raised. |
| 10240 | 10 | `ext4` | `/var` | primary | Contains *variable data files* ([more information](https://refspecs.linuxfoundation.org/FHS_3.0/fhs/ch05.html)). Help Links: [[1]](https://access.redhat.com/discussions/641923), [[2]](https://www.linuxquestions.org/questions/linux-general-1/var-folder-size-4175441329/#:~:text=Regular%20desktop%20eats%20between%20800Mb,%2Fvar%20about%202%2D3GiB.) |
| 5120 | 5 | `ext4` | `/tmp` | primary |  |
| 15360 | 15 | `ext4` | `/usr` | primary | Destination for all executable files, user binaries their documentations, headers, etc. |
| 148000 | 144.53125 | `ext4` | `/home` | primary |  |
| 920 | - | `ext4` | `/opt` | primary |  |
