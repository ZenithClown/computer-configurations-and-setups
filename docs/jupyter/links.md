Links that can be used to install/configure Tensor Flow in Linux (Pop! OS) with GPU Support.

https://www.tensorflow.org/install/docker # not yet working
https://github.com/NVIDIA/nvidia-docker   # nvidia-docker components, check installation instructions:

```zsh
# https://docs.nvidia.com/datacenter/cloud-native/container-toolkit/install-guide.html#docker
# tactically we should use $ID - however, `pop os` is not yet supported:
# https://nvidia.github.io/libnvidia-container/
# however, pop is based on ubuntu/debian, thus replaced the $ID
distribution=$(. /etc/os-release;echo ubuntu$VERSION_ID) \
      && curl -fsSL https://nvidia.github.io/libnvidia-container/gpgkey | sudo gpg --dearmor -o /usr/share/keyrings/nvidia-container-toolkit-keyring.gpg \
      && curl -s -L https://nvidia.github.io/libnvidia-container/$distribution/libnvidia-container.list | \
            sed 's#deb https://#deb [signed-by=/usr/share/keyrings/nvidia-container-toolkit-keyring.gpg] https://#g' | \
            sudo tee /etc/apt/sources.list.d/nvidia-container-toolkit.list

# you may also specify `$VERSION_ID` manually, thus, code can be reduced to:
# currently 22.04 LTS is available
distribution=ubuntu22.04 \
      && curl -fsSL https://nvidia.github.io/libnvidia-container/gpgkey | sudo gpg --dearmor -o /usr/share/keyrings/nvidia-container-toolkit-keyring.gpg \
      && curl -s -L https://nvidia.github.io/libnvidia-container/$distribution/libnvidia-container.list | \
            sed 's#deb https://#deb [signed-by=/usr/share/keyrings/nvidia-container-toolkit-keyring.gpg] https://#g' | \
            sudo tee /etc/apt/sources.list.d/nvidia-container-toolkit.list
```

Note the additional configuration and details:

```zsh
> neofetch
             /////////////                dpramanik@pop-os 
         /////////////////////            ---------------- 
      ///////*767////////////////         OS: Pop!_OS 22.04 LTS x86_64 
    //////7676767676*//////////////       Host: 82AU Lenovo Legion 5 15IMH05 
   /////76767//7676767//////////////      Kernel: 5.17.5-76051705-generic 
  /////767676///*76767///////////////     Uptime: 2 hours, 36 mins 
 ///////767676///76767.///7676*///////    Packages: 1951 (dpkg), 23 (flatpak) 
/////////767676//76767///767676////////   Shell: zsh 5.8.1 
//////////76767676767////76767/////////   Resolution: 1360x768, 1920x1080 
///////////76767676//////7676//////////   DE: GNOME 42.0 
////////////,7676,///////767///////////   WM: Mutter 
/////////////*7676///////76////////////   WM Theme: Pop 
///////////////7676////////////////////   Theme: Pop-dark [GTK2/3] 
 ///////////////7676///767////////////    Icons: Pop [GTK2/3] 
  //////////////////////'////////////     Terminal: gnome-terminal 
   //////.7676767676767676767,//////      CPU: Intel i7-10750H (12) @ 2.600GHz 
    /////767676767676767676767/////       GPU: NVIDIA 01:00.0 NVIDIA Corporation TU117M 
      ///////////////////////////         Memory: 6855MiB / 15908MiB 
         /////////////////////
             /////////////                                        
                                                                  

> docker --version
Docker version 20.10.16, build aa7e414
> docker compose version
Docker Compose version v2.5.0
> python3 --version
Python 3.10.4
```

Continue installation as:
```
sudo apt-get update
sudo apt-get install -y nvidia-docker2
sudo systemctl restart docker
```

Note, checking and verification of nvidia drivers failing. Directly move to installation of other components for python/pip:
```zsh
sudo apt install python3-pip
pip install virtualenv # for managing virtual ennvironment

# now let's create a virtual env to test tensorflow capabilities
mkdir tensorflow-gpu
cd tensorflow-gpu
python3 -m virtualenv venv
source venv/bin/activate
pip install tensorflow

# in addition also install the following:
sudo apt install libcudart11.0 libcublas11 libcublaslt11 libcufft10 libcurand10 libcusolver11 libcusparse11

# this also requires cudnn8, for this follow the link:
# https://stackoverflow.com/a/69302029/6623589
wget https://developer.download.nvidia.com/compute/cuda/repos/ubuntu2004/x86_64/cuda-ubuntu2004.pin
sudo mv cuda-ubuntu2004.pin /etc/apt/preferences.d/cuda-repository-pin-600

# using apt-key inssues permanent warning messages as for deprication warning
sudo apt-key adv --fetch-keys https://developer.download.nvidia.com/compute/cuda/repos/ubuntu2004/x86_64/3bf863cc.pub
sudo add-apt-repository "deb https://developer.download.nvidia.com/compute/cuda/repos/ubuntu2004/x86_64/ /"
sudo apt-get update
sudo apt-get install libcudnn8
sudo apt-get install libcudnn8-dev
```
