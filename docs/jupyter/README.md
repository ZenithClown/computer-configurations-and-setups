# Jupyter Notebook and Anaconda Guide

Some useful commands and Installation Spsecific to **Jupyter/Conda** Environment. If you have an _[Anaconda Cloud](https://anaconda.org/account/login)_ account, you can also upload your Environments and save them for Later Use!! [More Deatils](https://docs.anaconda.com/anaconda-cloud/user-guide/tasks/work-with-environments/) on how to create a YML file is available in documentations, and my personal built environments are available [here](https://anaconda.org/ZenithClown/environments).

**Guide** to change initial home directory for jupyter can be found [here](https://stackoverflow.com/questions/35254852/how-to-change-the-jupyter-start-up-folder).

## Conda Commands
| command | Usage and Definations | Variants (if any) | Remarks |
| :--- | :---: | :--- | :---: |
| `conda env list` | Lists all conda-environment | | |
| `conda activate <env-name>` | Switch to another Environment | | |
| `conda deactivate` | Switch to *base* | | |
| `jupyter kernelspec list` | Lists all the Kernals Installed with Jupyter | | |
| `conda create -n <env-name>` | Creates a New Conda-Environment | **Create an Environment with a Specific Python Version** <br> `conda create -n <env-name> pip python=<version>` <br> **Clone and Create a New Environment** <br> `conda create -n <env-name> --clone <env-to-clone>` | |
| `conda remove -n <env-name> --all` | Completely Delete an Environment || |
| `>> pip install ipykernel` <br> `>> python -m ipykernel install --user --name <env-name> --display-name "<display-name>"` | Add a New Kernel to Jupyter-Notebook | | Install `kernelspec` after activating the same-environment using `conda` |
| `jupyter kernelspec uninstall <kernel-name>` | Remove a Jupyter-Kernel | | |
| `pip show <pkg-name>` | Show Information about a PKG (if installed) | | |
