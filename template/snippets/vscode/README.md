<div align="center">

<h1 align = "center">VS Code Snippets</h1>
<p align = "center">Snippets to Code Fast</p>

</div>

Like most powerful IDEs, VS Code allows the use of snippets. This directory hosts multiple snippets that I personally use. More information on [snippets](https://code.visualstudio.com/docs/editor/userdefinedsnippets) for VS Code is available in documentations.
