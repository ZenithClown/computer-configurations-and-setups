# -*- encoding: utf-8 -*-

"""
Generate VS Code `python` Snippets File

VS Code saves the python snippets under `python.json` file, and
this code generates the same from all the defined JSON file, or
a set of defined files.
"""

import argparse
import glob
import json

if __name__ == "__main__":
    parser = argparse.ArgumentParser() # parses cli arguments

    # list of defined arguments
    parser.add_argument("-f", "--files", help = "List of Files (w/o) Directory to Parse")
    parser.add_argument("-o", "--outfile", help = "Output File Name (with directory)")

    # get list of arguments, and set default values
    args = parser.parse_args()

    if args.files:
        files = [f"{f}.json" for f in args.files.split(",")]
    else:
        # process for all json file
        files = glob.glob("*.json")
    
    outfile = args.outfile or "python.json"
    
    # merge all the snippets into one `dictionary` object
    snippets = dict()
    for file in files:
        snippets |= json.load(open(file, "r"))

    # save the snippets to `outfile`
    with open(outfile, "w") as f:
        json.dump(snippets, f, indent = 2, sort_keys = False, default = str)
