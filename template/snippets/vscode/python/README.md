<div align="center">

<h1 align = "center">VS Code Snippets</h1>
<p align = "center"><code>python</code> Snippets</p>

</div>

Personally, **`python`** is my go to language. Original `python.json` File Contents:

```
{
	// Place your snippets for python here. Each snippet is defined under a snippet name and has a prefix, body and 
	// description. The prefix is what is used to trigger the snippet and the body will be expanded and inserted. Possible variables are:
	// $1, $2 for tab stops, $0 for the final cursor position, and ${1:label}, ${2:another} for placeholders. Placeholders with the 
	// same ids are connected.
	// Example:
	// "Print to console": {
	// 	"prefix": "log",
	// 	"body": [
	// 		"console.log('$1');",
	// 		"$2"
	// 	],
	// 	"description": "Log output to console"
	// }
}
```

## List of Snippets

I've segregated snippets into different files based on different use cases. Simply use `python generator.py` to create `python.json` file from all the files, or from a set of defined files from command line as `python generator.py file_1,file2,...,file_n` (without extension). **NOTE:** the snippets file will be created in the same directory.

* **analytics.json** : A snippets file dedicated towards data analytics libraries like [`numpy`](https://numpy.org/), [`pandas`](https://pandas.pydata.org/), etc.
* **files.json** : List of snippets dedicated towards reading/writing different formats of file (like JSON, text).
