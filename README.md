<div align="center">

<img src = "./meta/icons/generic/images/gear.png" height = "30px" align = "right" />
<h1 align = "center">Knitty Gritty Computer Configurations</h1>

<b>Connect with me at:</b>
<a href = "https://www.linkedin.com/in/dpramanik/"><img height="16" width="16" src="https://unpkg.com/simple-icons@v3/icons/linkedin.svg"/></a>
	<a href = "https://github.com/ZenithClown"><img height="16" width="16" src="https://unpkg.com/simple-icons@v3/icons/github.svg"/></a>
	<a href = "https://gitlab.com/ZenithClown/"><img height="16" width="16" src="https://unpkg.com/simple-icons@v3/icons/gitlab.svg"/></a>
	<a href = "https://www.researchgate.net/profile/Debmalya_Pramanik2"><img height="16" width="16" src="https://unpkg.com/simple-icons@v3/icons/researchgate.svg"/></a>
	<a href = "https://www.kaggle.com/dPramanik/"><img height="16" width="16" src="https://unpkg.com/simple-icons@v3/icons/kaggle.svg"/></a>
	<a href = "https://app.pluralsight.com/profile/Debmalya-Pramanik/"><img height="16" width="16" src="https://unpkg.com/simple-icons@v3/icons/pluralsight.svg"/></a>
	<a href = "https://stackoverflow.com/users/6623589/"><img height="16" width="16" src="https://unpkg.com/simple-icons@v3/icons/stackoverflow.svg"/></a>

</div>

<div align = "justify">

🤓 Being a tech-enthusiast 💻, I spent much time and efforts in polishing ⚙️ my own working environment to perfection! Well, with experience 🧑 and maturity many settings and configurations 🛠️ are revamped and modified. This is a *dedicated repository* 🧲 that helps me keep track of `environments`, settings and configurations of various softwares and/or perform a *quick setup* when things ⚠️ fails!

<div align = "center">

🤩 **_You are free to use any content, and use it in whatever way you like! Do put a 🌟 if you find any contents useful!_** 🤩

⚠⚠ **This Repository DOES NOT REQUIRE Any External Contributions** ⚠⚠

</div>

Using a bunch of 💾 softwares, means a *lot of configurations* which are sorted out in this directory. The 🔴🔥 **`settings` 🗂️ directory** 🔥🔴 is organised by application names, and settings/configuration files. In additions, other important files (`.gitconfig` etc.) are stored. While, the 🔴🔥 **`meta` 🗂️ directory** 🔥🔴 can be used to create project structure and clone/restore *projects* 📚 from various sources.

## Getting Started

💻⏳ *"A time consuming & lengthy process"* I prefer to use [Linux Mint](https://linuxmint.com/) as my primary development machine, while [Windows 10](https://www.microsoft.com/en-in/software-download/windows10) is the goto for [NVIDIA](https://www.nvidia.com/en-in/) and running Neural Network Applications! ⚠️ As *many other top class windows flaws*, naming a *user* directory is also a pain staking task! By *default* windows [truncates username](https://answers.microsoft.com/en-us/windows/forum/all/windows-user-profile-name-truncated-to-5/79d2843a-286a-455c-8826-344e8438937a) 🙄🤔😐😑😒😬, anyways, to fix this:

  * while setup **do not connect to internet**, and
  * create **a local account** (put the name you want as `username`, like `debmalya`)
  * finally, once setup is complete **login** with your Microsoft A/C to sync system.

Ya! I am meticulous and there is a **_triggered panic button_** ⛔⚠️ if things falls *out of line*! 😒 This INCLUDES *temperature elevation* when Windows/Microsoft truncates my username to `debma`! 😑🤒 Anyway, let's get started with the setup.

### PREQUISTES

Configure windows with basic requirements and softwares that is required for the development environment. Softwares including `.NET Framework`, `Visual C++ Redistributables` and `Visual Studio Community Edition (2017, 2022)` is available under *segate backup drive* for fast-local installation.

#### Visual Studio Community Edition (2017, 2022)

Microsoft's Visual Studio Community Edition along with *individual components* are required for Deep Learning Frameworks. First, **install/update [NVIDIA Driver](https://www.nvidia.com/Download/index.aspx)** for the current system, followed by installation:

1. **Install `2017` Version** Directly - NO additional components are required.
2. **Install `2022` Version** - *SELECT* the following individual components from the menu (approx. `45 GB` download) before proceeding:
    * .NET 6.0 (Long Term Support),
    * .NET 7.0 Runtime,
    * .NET Framework 4.6.1 targeting pack
    * .NET Framework 4.7.2 targeting pack
    * .NET Framework 4.8 SDK
    * *Install the C++/CMake* from "Compilers, Build Tools, and Runtimes":
      - .NET Compiler Platform SDK
      - C# and Visual Basic Roslyn Compilers
      - C++ Redistributables, CLI Support, Build Tools - **ALL** (14.16-17.4)
      - MSBuild
      - MSVC v140-3 - VS 2015-22 C++ Build Tools, Spectre-Mitigated Libs, `x64/x86` Build Tools - **ALL** with ARM Build Tools (latest)
      - Python 3 64-bit (3.9.7) : Installed by default, do not add this to `PATH` as functionality is not added
    * Development Activities:
      - C# and Visual Basic
      - C++ CMake Tools for Linux
      - C++ for Linux Development

<div align = "center">
<details>
<summary><b><i>Validate VS Community Edition Installation</i></b></summary>

```shell
# `x64` Compatibility Verification
# 💥 `shift + right click` > open power shell here

PS C:\Program Files\Microsoft Visual Studio\2022\Community\VC\Auxiliary\Build> .\vcvars64.bat
  **********************************************************************
  ** Visual Studio 2022 Developer Command Prompt v17.4.1
  ** Copyright (c) 2022 Microsoft Corporation
  **********************************************************************
  [vcvarsall.bat] Environment initialized for: 'x64'
```

</details>
</div>

#### NVIDIA `CUDA` and `cuDNN` Configurations

A particular *compatible version* of CUDA and cuDNN is required to run deep learning applications ([supported versions](https://www.tensorflow.org/install/source#gpu))! 😅 Once `prequisites` and installation of Visual Studio Community Edition (2017 & 2022) is done, then install `CUDA 11.2.2_461.33` and `cuDNN 8.1.1.33` as below:

* Install **`CUDA 11.2.2_461.33`**. ⚠️ If any components is missing from Visual Studio Community Edition a WARNING is displayed and `insights` is not installed. Check system and installation instructions.
* Copy the contents of **`cuDNN 8.1.1.33`** for `CUDA 11.2` as in folder, i.e. copy the contents of `cuDNN/cuda/bin` to `<...>/Program Files/NVIDIA GPU Computing Toolkit/CUDA/v11.2/bin` etc.
* Configure the PATH Variables, and include the *system environment variables* as required.

<div align = "center">
<details>
<summary><b>Check Details of <code>${PATH}</code> & Environment Variables</b></summary>

<div align = "left">

```shell
# considering default installation path, the following elements must
# be present in the ${PATH} variable
C:\Program Files\NVIDIA GPU Computing Toolkit\CUDA\v11.2\bin
C:\Program Files\NVIDIA GPU Computing Toolkit\CUDA\v11.2\libnvvp
C:\Program Files\NVIDIA GPU Computing Toolkit\CUDA\v11.2\include
C:\Program Files\NVIDIA GPU Computing Toolkit\CUDA\v11.2\extras\CUPTI\lib64
C:\Program Files\NVIDIA GPU Computing Toolkit\CUDA\v11.2\extras\CUPTI\include

# additionally, also add and check if the insights engine is added to ${PATH}
C:\Program Files\NVIDIA Corporation\NVIDIA NvDLISR
C:\Program Files (x86)\NVIDIA Corporation\PhysX\Common
C:\Program Files\NVIDIA Corporation\Nsight Compute 2020.3.1\

# in addition, add the new variables as:
export CUDA_PATH = "C:\Program Files\NVIDIA GPU Computing Toolkit\CUDA\v11.2"

export CUDA_INC_PATH = "%CUDA_PATH%\include"
export CUDA_LIB_PATH = "%CUDA_PATH%\bin\x64"
```

</div>
</details>
</div>

## Software Installations & Settings Options

Check [**`setings`**](./settings/) for additional software *direct* download links, and *configuration* file with detailed instructions. In addition, here is a list of programs that can be configured using this repository:

  1. [`.git` Confiruration](./settings/git/) - includes example of setting up different users per directory, and aliases.
  2. [Aliases for Linux](./settings/linux/) - includes WSL alias file.
  3. ~[Sublime Text 3](./settings/sublime/) - settings and [package control setup, by Corey Schafer](https://www.youtube.com/watch?v=xFciV6Ew5r4&t=314s) instructions.~
  4. [`jupyter` notebook](./settings/jupyter/) - can be configured with *style-sheets* using CSS. This can be from `~/.jupyter/custom/custom.css` and edit the contents as required. Example or links to more styles added in the file. If the file does not exists already, create the same. Currently, the css file increases the *cell-width* in all notebook.
  5. [`matplotlib` styles](./settings/python/matplotlib/) - a custom style-sheet specific to matplotlib can be defined for all figures. This can be done via any of the two methods:
      * either, define a custom style-sheet for a project, say `./images/styles.mplstyle`
      * or, you can define global style inside matplotlib config directoy, which is available in the following directory:

```bash
# if directory does not exists, create one
# https://matplotlib.org/tutorials/introductory/customizing.html

~/users/<user-name>/.matplotlib/stylelib/ # in Windows-Operating System, or
~/.config/matplotlib/stylelib/ # in Linux Systems
```

  6. **NBExtensions** : Jupyter provides a list of [`extensions`](https://jupyter-contrib-nbextensions.readthedocs.io/en/latest/install.html) to *make orur life easier*! Enable extensions from `anaconda prompt` as mentioned below. In addition check [snippets](./template/snippets/jupyter/) for configuring code snippets.

```python
# in the anaconda prompt `base` environment, run:
pip install jupyter_contrib_nbextensions && jupyter contrib nbextension install
```

  7. [WSL](./settings/wsl/) - for setting up `ubuntu` in WSL-2, settings for the new [**`windows terminal`**](https://apps.microsoft.com/store/detail/windows-terminal/9N0DX20HK701?hl=en-in&gl=in) and other informations. Detals of adding `anaconda` profile is added in `settings.json` however check [this](https://dev.to/azure/easily-add-anaconda-prompt-in-windows-terminal-to-make-life-better-3p6j).

### TensorFlow with Keras Installation

🔴⚠️ First complete prequisites section followed by [`tensorflow`](https://www.tensorflow.org/install) installation. Create a [new `conda` environment](./docs/jupyter/), and install with `pip install tensorflow==2.9`. Verify the installation (and GPU compatibility):

```python
import tensorflow as tf
print(f"Tensorflow Version: {tf.__version__}")
>> Tensorflow Version: 2.8.0

# check physical devices
tf.config.list_physical_devices()
>> [PhysicalDevice(name='/physical_device:CPU:0', device_type='CPU'), PhysicalDevice(name='/physical_device:GPU:0', device_type='GPU')]
```

**NOTE:** CUDA 11.2 and cuDNN 8.1 is required for TensorFlow 2.9, the same version of CUDA and cuDNN is supported by PyTorch.
  * [PyTorch Version Compatibility](https://pytorch.org/get-started/previous-versions/) - ? check version for CUDA 11.2
  * [PyTorch Installations](https://pytorch.org/get-started/locally/) - get started locally
  * [TensorFlow Requirements](https://www.tensorflow.org/install/pip#windows-wsl2) : ⚠️ DO NOT INSTALL `cudatoolkit` and `cudnn` via `conda install`, follow instructions as above 👆 and check [install instructions](https://www.tensorflow.org/install).

## Additional Softwares

[SQLite3 ODBC Driver](http://www.ch-werner.de/sqliteodbc/)

</div>

---

<div align = "justify">

This repository is developed for own personal usage. Unless specifically mentioned [issues](https://gitlab.com/ZenithClown/computer-configurations-and-setups/-/issues) with a label `help-wanted` is mentioned, any other PR will be automatically rejected. If you want to use the repository/any of its content, then please [fork](https://gitlab.com/ZenithClown/computer-configurations-and-setups/-/forks/new) the repository and make the required changes.

</div>

<div align = "center">

**_"polish, setup, configure, format, restore"_**

</div>
